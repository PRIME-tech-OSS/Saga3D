#include <Saga.h>
#include <chrono>
#include <cmath>
#include <random>

using namespace saga;
using namespace core;
using namespace video;

struct FireParticleInstance {
  glm::vec3 position;
  float age;
  glm::vec3 velocity;
  float life;
  glm::vec3 pad;
  float start;
};

struct Matrix {
  glm::mat4 Model;
  glm::mat4 View;
  glm::mat4 Projection;
};

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {800, 600}, 16, false, false, false);

  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  constexpr std::array<float, 12> vertices = {
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    -0.5f, 0.5f, 0.0f,
    0.5f, 0.5f, 0.0f,
  };

  auto vertexBufferInfo = driver->createShaderBuffer();
  vertexBufferInfo.Size = sizeof(float) * vertices.size();
  auto vertexBuffer = driver->createResource(std::move(vertexBufferInfo));
  driver->updateShaderBuffer(vertexBuffer, vertices.data());

  constexpr auto particleCount = 1024;
  auto instanceBufferInfo = driver->createShaderBuffer();
  instanceBufferInfo.Size = sizeof(FireParticleInstance) * particleCount;
  auto instanceBuffer = driver->createResource(std::move(instanceBufferInfo));

  std::vector<FireParticleInstance> vec;
  vec.reserve(particleCount);
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> disPosX(-0.05, 0.05);
  std::uniform_real_distribution<> disVel(0.3, 1.0);
  std::uniform_real_distribution<> disStart(0.3, 0.7);
  for (int i = 0; i < particleCount; ++i) {
    vec.push_back({});
    auto& p = vec.back();
    p.position.x = disPosX(gen);
    p.position.y = 0.f;
    p.position.z = -0.2f;
    p.age = 0.f;
    p.life = 0.1f;
    p.velocity.x = 0.f;
    p.velocity.y = disVel(gen);
    p.velocity.z = 0.f;
    p.start = disStart(gen);
  }
  driver->updateShaderBuffer(instanceBuffer, vec.data());

  auto computeShaderInfo = driver->createShader();
  computeShaderInfo.CSource = R"(
    #version 450
    layout (local_size_x = 32, local_size_y = 1, local_size_z = 1) in;

    layout(push_constant) uniform Frame
    {
      int particleCount;
      float time; // last frame delta time in seconds
    } frame;

    struct FireParticleInstance {
      vec3 position;
      float age;
      vec3 velocity;
      float life;
      vec3 pad;
      float start;
    };

    layout(std430, binding = 0) buffer StorageBuffer
    {
      FireParticleInstance particles[];
    };

    float rand(vec2 co){
      return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
    }

    void main() {
      if (gl_GlobalInvocationID.x < frame.particleCount) {
        FireParticleInstance p = particles[gl_GlobalInvocationID.x];
        p.age += frame.time;
        if (p.age > p.start) {
          if (p.age >= p.start + p.life) {
            p.position.y = 0.f;
            p.age = 0.f;
          } else {
            p.position.y += p.velocity.y * frame.time;
          }
        }
        particles[gl_GlobalInvocationID.x] = p;
     }
    }
  )";

  auto time = 0.f;
  auto pushConstantInfo = driver->createPushConstant();
  pushConstantInfo.Size = sizeof(time);
  auto constant = driver->createResource(std::move(pushConstantInfo));

  auto computePipelineInfo = driver->createPipeline();
  computePipelineInfo.Shaders = driver->createResource(std::move(computeShaderInfo));
  auto computePipeline = driver->createResource(std::move(computePipelineInfo));
  const int groupCount = std::ceil(particleCount / 32.0);

  auto mesh = std::make_shared<scene::SMesh>();
  {
    auto meshBuffer = std::make_unique<scene::CGPUMeshBuffer>();
    meshBuffer->setVertexBuffer(vertexBuffer);
    meshBuffer->setVertexCount(4);
    meshBuffer->setPrimitiveType(scene::E_PRIMITIVE_TYPE::TRIANGLE_STRIP);
    meshBuffer->setBindingLocation(0);
    meshBuffer->setInstanceCount(particleCount);
    mesh->addMeshBuffer(std::move(meshBuffer));
  }
  {
    auto meshBuffer = std::make_unique<scene::CGPUMeshBuffer>();
    meshBuffer->setVertexBuffer(instanceBuffer);
    meshBuffer->setVertexCount(particleCount);
    meshBuffer->setBindingLocation(1);
    mesh->addMeshBuffer(std::move(meshBuffer));
  }
  auto node = smgr->createSceneNode(mesh);

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec3 position;
    layout (location = 1) in vec4 positionAge;
    layout (location = 2) in vec4 velocityLife;
    layout (location = 3) in vec4 start;

    layout (binding = 0) uniform Matrix
    {
      mat4 Model;
      mat4 View;
      mat4 Projection;
    } matrix;

    // layout (location = 0) out float colorIndex;

    void main() {
      // colorIndex = (positionAge.w - start.w) / velocityLife.w;
      // gl_Position = matrix.Projection * matrix.View * matrix.Model * vec4(position + positionAge.xyz, 1.0);
      gl_Position = vec4(position + positionAge.xyz, 1.0);
      // gl_PointSize = 5.0f;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450
    // layout (location = 0) in float colorIndex;
    layout (location = 0) out vec4 fragColor;

    const vec3 colors[5] = vec3[] (
      vec3(1.0, 0.0, 0.0),
      vec3(1.0, 0.35, 0.0),
      vec3(1.0, 0.6, 0.0),
      vec3(1.0, 0.81, 0.0),
      vec3(1.0, 0.91, 0.03)
    );

    void main() {
      // fragColor = vec4(colors[clamp(int(colorIndex), 0, 4)], 1.0);
      fragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Rasterizer.CullMode = video::E_CULL_MODE::FRONT_FACE;
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));
  pipelineInfo.VertexBindingCount = 2;

  pipelineInfo.Layout.Attributes[0][0] = {
    E_ATTRIBUTE_TYPE::CUSTOM,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };

  pipelineInfo.Layout.Attributes[1][0] = {
    E_ATTRIBUTE_TYPE::CUSTOM,
    E_ATTRIBUTE_FORMAT::FLOAT4,
  };
  pipelineInfo.Layout.Attributes[1][1] = {
    E_ATTRIBUTE_TYPE::CUSTOM,
    E_ATTRIBUTE_FORMAT::FLOAT4,
  };
  pipelineInfo.Layout.Attributes[1][2] = {
    E_ATTRIBUTE_TYPE::CUSTOM,
    E_ATTRIBUTE_FORMAT::FLOAT4,
  };

  auto matrixUniformInfo = driver->createShaderUniform();
  matrixUniformInfo.Size = sizeof(Matrix);
  auto matrixUniform = driver->createResource(std::move(matrixUniformInfo));

  auto passInfo = driver->createRenderPass();
  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerNode(node, pass);

  auto& cam = smgr->addCameraSceneNode();
  cam->setPosition({0.f, 0.1f, 0.f});
  cam->setTarget({0.f, 0.f, -1.f});

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    if (device->isWindowActive()) {
      smgr->animate();
      driver->begin();

      driver->bindComputePipeline(computePipeline);
      driver->updatePushConstant(constant, &particleCount, 0, sizeof(particleCount));
      driver->updatePushConstant(constant, &time, sizeof(particleCount), sizeof(time));
      driver->bindShaderBuffer(instanceBuffer, 0);
      driver->dispatchComputePipeline(groupCount, 1, 1);

      driver->end();
      driver->submit();

      driver->begin();
      driver->beginPass(pass);
      Matrix matrix;
      matrix.Model = node->getAbsoluteTransformation();
      matrix.View = cam->getViewMatrix();
      matrix.Projection = cam->getProjectionMatrix();
      driver->updateShaderUniform(matrixUniform, &matrix);
      driver->bindShaderUniform(matrixUniform, 0);
      driver->draw();
      driver->endPass();
      driver->end();
      driver->submit();
      driver->present();
    } else
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    time = millis / 1000.f;
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D Particle - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

