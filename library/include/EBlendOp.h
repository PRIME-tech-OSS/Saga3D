#ifndef __E_BLEND_OP_H_INCLUDED__
#define __E_BLEND_OP_H_INCLUDED__

namespace saga
{
namespace video
{

enum class E_BLEND_OP
{
  ADD,
  SUBTRACT,
  REVERSE_SUBTRACT,
  MIN,
  MAX
};

} // namespace video
} // namespace saga

#endif // __E_BLEND_OP_H_INCLUDED__

