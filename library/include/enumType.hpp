#ifndef __ENUM_TYPE_H_INCLUDED__
#define __ENUM_TYPE_H_INCLUDED__

#include <type_traits>

namespace saga
{

namespace core
{

template <typename E>
constexpr inline auto enumToPOD(E e) noexcept
{
    return static_cast<std::underlying_type_t<E>>(e);
}

} // namespace core
} // namespace saga

#endif

