#ifndef __E_PIXEL_FORMAT_H_INCLUDED__
#define __E_PIXEL_FORMAT_H_INCLUDED__

namespace saga
{
namespace video
{

  //! Enumeration for all primitive types there are.
  enum class E_PIXEL_FORMAT
  {
    INVALID,
    BGRA8, //! Vulkan swapchain format
    RGBA8,
    RGB8,
    RGBA4,
    R5G6B5,
    A1R5G5B5,
    A2R10G10B10,
    RGB32F,
    RGBA32F,
    RGB16F,
    RGBA16F,
    RGBA16,
    R8_UNORM,
    R16F,
    RG_16UINT,
    RG_32F,
    R32_UINT,
    R32_SINT,
    R32F,
    R32G32_UINT,
    R32G32_SINT,
    L8,
    DXT1,
    DXT3,
    DXT5,
    STENCIL8,
    DEPTH_16,
    DEPTH_24,
    DEPTH_32,
    DEPTH_16_STENCIL8,
    DEPTH_24_STENCIL8,
    DEPTH_32_STENCIL8,
    PVRTC2_RGB,
    PVRTC4_RGB,
    PVRTC2_RGBA,
    PVRTC4_RGBA,
    ETC2_RGB8,
    ETC2_SRGB8,
    UNKNOWN
  };

} // namespace scene
} // namespace saga

#endif

