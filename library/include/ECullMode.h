#ifndef __E_CULL_MODE_H_INCLUDED__
#define __E_CULL_MODE_H_INCLUDED__

namespace saga
{
namespace video
{

enum E_CULL_MODE
{
  NONE,
  BACK_FACE,
  FRONT_FACE,
  FRONT_BACK_FACE,
};

} // namespace scene
} // namespace saga

#endif

