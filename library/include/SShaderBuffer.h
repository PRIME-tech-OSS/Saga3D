#ifndef __SSHADER_BUFFER_H_INCLUDED__
#define __SSHADER_BUFFER_H_INCLUDED__

#include "SGPUResource.h"
#include <memory>

namespace saga
{
namespace video
{
  struct SShaderBuffer : public SGPUResource
  {
    //! Size of this buffer
    std::size_t Size = 0;
    //! CPU read from GPU
    bool ReadBack = false;
    //! Allow binding as vertex buffer
    bool VertexBufferBind = false;
    //! Allow binding as index buffer
    bool IndexBufferBind = false;
  };

  using ShaderBufferHandle = SGPUResource::HandleType;

} // namespace scene
} // namespace saga

#endif // __SSHADER_BUFFER_H_INCLUDED__

