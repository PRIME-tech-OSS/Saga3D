#include "CVulkanDriver.h"
#include "SRenderPassState.h"
#include "SRenderPass.h"
#include "CSceneManager.h"
#include "IMeshSceneNode.h"
#include "IAnimatedMesh.h"
#include "IAnimatedMeshSceneNode.h"
#include "IMeshBuffer.h"
#include "CGPUMeshBuffer.h"
#include "ICameraSceneNode.h"
#include "backend/vulkan/VulkanTypes.h"
#include <CSagaDeviceSDL.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#include <SDL2/SDL_vulkan.h>
#include <Core/Swapchain.h>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>

namespace saga
{
namespace video
{

CVulkanDriver::CVulkanDriver(const SDeviceCreationParameters& params, CSagaDeviceSDL& device)
  :  CVideoDriver(params), Device(device)
{
  if (CreationParams.DriverType == E_DRIVER_TYPE::VULKAN_HEADLESS)
  {
    ManageFramebuffer = false;
  }
}

std::uint32_t CVulkanDriver::getWidth() const
{
  return Device.getWidth();
}

std::uint32_t CVulkanDriver::getHeight() const
{
  return Device.getHeight();
}

CVulkanDriver::~CVulkanDriver()
{
  //TODO: more vulkan cleanup
  // Wait for all device operations to complete.
  vezDeviceWaitIdle(VKDevice);
  
  // Call application's Cleanup method.
  // Cleanup();
  
  // Destroy framebuffer.
  if (ManageFramebuffer)
  {
    if (DefaultFramebuffer.handle)
    {
      vezDestroyFramebuffer(VKDevice, DefaultFramebuffer.handle);
      vezDestroyImageView(VKDevice, DefaultFramebuffer.colorImageView);
      vezDestroyImageView(VKDevice, DefaultFramebuffer.depthStencilImageView);
      vezDestroyImage(VKDevice, DefaultFramebuffer.colorImage);
      vezDestroyImage(VKDevice, DefaultFramebuffer.depthStencilImage);
    }
  }

  // Destroy the swapchain.
  vezDestroySwapchain(VKDevice, Swapchain);

  // Destroy device.
  vezDestroyDevice(VKDevice);

  // Destroy surface.
  vkDestroySurfaceKHR(Instance, Surface, nullptr);

  // Destroy instance.
  vezDestroyInstance(Instance);
}

bool CVulkanDriver::initDriver()
{
  // Enumerate all available instance layers.
  uint32_t layerCount = 0;
  vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

  std::vector<VkLayerProperties> layerProperties(layerCount);
  vkEnumerateInstanceLayerProperties(&layerCount, layerProperties.data());

  constexpr auto VK_VALIDATION_LAYER_NAME = "VK_LAYER_KHRONOS_validation";

  bool standardValidationFound = false;
  for (auto prop : layerProperties)
  {
    if (std::string(prop.layerName) == VK_VALIDATION_LAYER_NAME)
    {
      standardValidationFound = true;
      break;
    }
  }

  // TODO: check and load extensions
  // Initialize a Vulkan instance with the validation layers enabled and extensions required by glfw.
  constexpr uint32_t instanceExtensionCount = 3;
  const char* instanceExtensions[] =
  {
    "VK_KHR_surface",
    "VK_KHR_get_physical_device_properties2",
    #ifdef __linux__
      #ifdef __ANDROID__
        "VK_KHR_android_surface"
      #else
        "VK_KHR_xlib_surface"
      #endif
    #elif defined _WIN32
      "VK_KHR_win32_surface"
    #elif defined __APPLE__
      #if TARGET_OS_IPHONE == 1
        "VK_MVK_ios_surface"
      #else
        "VK_MVK_macos_surface"
      #endif
    #endif
  };

  std::vector<const char*> instanceLayers;
  if (EnableValidationLayers && standardValidationFound)
    instanceLayers.push_back(VK_VALIDATION_LAYER_NAME);
  
  VezApplicationInfo appInfo = { nullptr, "Saga3D", VK_MAKE_VERSION(1, 0, 0), "", VK_MAKE_VERSION(0, 0, 0) };

  VezInstanceCreateInfo createInfo = {
    nullptr, &appInfo, static_cast<uint32_t>(instanceLayers.size()),
    instanceLayers.data(),
    instanceExtensionCount, instanceExtensions
  };

  auto result = vezCreateInstance(&createInfo, &Instance);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateInstance failed", result);
    return false;
  }

  // Enumerate all attached physical devices.
  uint32_t physicalDeviceCount = 0;
  vezEnumeratePhysicalDevices(Instance, &physicalDeviceCount, nullptr);
  if (physicalDeviceCount == 0)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "No Vulkan physical devices found");
    return false;
  }

  std::vector<VkPhysicalDevice> physicalDevices(physicalDeviceCount);
  vezEnumeratePhysicalDevices(Instance, &physicalDeviceCount, physicalDevices.data());

  VkPhysicalDevice discreteGPU = VK_NULL_HANDLE;
  VkPhysicalDevice integratedGPU = VK_NULL_HANDLE;

  for (auto pd : physicalDevices)
  {
    VkPhysicalDeviceProperties properties = {};
    vezGetPhysicalDeviceProperties(pd, &properties);
    if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
    {
      discreteGPU = pd;
    }
    else if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
    {
      integratedGPU = pd;
    }
  }

  if (discreteGPU != VK_NULL_HANDLE)
  {
    PhysicalDevice = discreteGPU;
  }
  else if (integratedGPU != VK_NULL_HANDLE)
  {
    PhysicalDevice = integratedGPU;
  }
  else
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "No GPU found");
    return false;
  }

  vezGetPhysicalDeviceFeatures(PhysicalDevice, &DeviceFeatures);

  // Get the physical device information.
  VkPhysicalDeviceProperties properties = {};
  vezGetPhysicalDeviceProperties(PhysicalDevice, &properties);
  std::string deviceLog = "Graphics device: ";
  deviceLog += properties.deviceName;
  SDL_LogInfo(SDL_LOG_CATEGORY_RENDER, deviceLog.c_str());

  if (CreationParams.DriverType == E_DRIVER_TYPE::VULKAN_OVERLAY)
  {
    #ifdef __ANDROID__
    VkAndroidSurfaceCreateInfoKHR info{};
    info.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
    info.window = (ANativeWindow*) CreationParams.WindowId;
    info.flags = 0;
    result = vkCreateAndroidSurfaceKHR(Instance, &info, nullptr, &Surface);
    #endif // __ANDROID__

    #ifdef __linux__
    VkXlibSurfaceCreateInfoKHR info{};
    info.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
    info.flags = 0;
    info.dpy =  (Display*) CreationParams.DisplayId;
    info.window = *((Window*) CreationParams.WindowId);
    result = vkCreateXlibSurfaceKHR(Instance, &info, nullptr, &Surface);
    #endif // __linux__

    #ifdef WIN32
    VkWin32SurfaceCreateInfoKHR info{};
    info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    info.hwnd = (HWND) CreationParams.WindowId;
    info.hinstance = GetModuleHandle(nullptr);
    result = vkCreateWin32SurfaceKHR(Instance, &info, nullptr, &Surface);
    #endif // WIN32

    if (result != VK_SUCCESS)
    {
      SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Platform surface creation failed");
      return false;
    }
  
    DeviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
  }
  else if (CreationParams.DriverType == E_DRIVER_TYPE::VULKAN)
  {
    SDL_Window* window = Device.getSDLWindow();
    SDL_SysWMinfo info;
    SDL_VERSION(&info.version);
    SDL_GetWindowWMInfo(window, &info);

    // Create Vulkan surface
    if (SDL_Vulkan_CreateSurface(window, Instance, &Surface) == false)
    {
      SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Platform surface creation failed");
      return false;
    }
    DeviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
  }

#if VK_HEADER_VERSION > 110
  DeviceExtensions.push_back(VK_EXT_MEMORY_BUDGET_EXTENSION_NAME);
#endif

  // Create the Vulkan device handle.
  VezDeviceCreateInfo deviceCreateInfo = { nullptr, 0, nullptr, static_cast<uint32_t>(DeviceExtensions.size()), DeviceExtensions.data() };
  result = vezCreateDevice(PhysicalDevice, &deviceCreateInfo, &VKDevice);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateDevice failed");
    return false;
  }

  if (CreationParams.DriverType != E_DRIVER_TYPE::VULKAN_HEADLESS)
  {
    // Create the swapchain.
    VezSwapchainCreateInfo swapchainCreateInfo = {};
    swapchainCreateInfo.surface = Surface;
    swapchainCreateInfo.format = { VK_FORMAT_R8G8B8A8_SRGB, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
    swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchainCreateInfo.tripleBuffer = VK_TRUE;
    result = vezCreateSwapchain(VKDevice, &swapchainCreateInfo, &Swapchain);
    if (result != VK_SUCCESS)
    {
      SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateSwapchain failed");
      return false;
    }

    vezSwapchainSetVSync(Swapchain, CreationParams.Vsync ? VK_TRUE : VK_FALSE);

    auto vkSwapchain = reinterpret_cast<vez::Swapchain*>(Swapchain)->GetHandle();

    uint32_t actualImageCount = 0;
    if (vkGetSwapchainImagesKHR(VKDevice, vkSwapchain, &actualImageCount, nullptr) != VK_SUCCESS)
    {
      SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Failed to acquire number of swap chain images");
      exit(1);
    }

    if (actualImageCount == 0)
    {
      SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Number of swap chain images is 0");
      exit(1);
    }

    SwapchainImages.resize(actualImageCount);

    if (vkGetSwapchainImagesKHR(VKDevice, vkSwapchain, &actualImageCount, SwapchainImages.data()) != VK_SUCCESS) {
      SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Failed to acquire swap chain images");
      exit(1);
    }
  }

#ifdef SAGA_EXTRA_API_MEMORY_INFO
  MemoryBudgetProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT;
  MemoryProperties2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
  MemoryProperties2.pNext = &MemoryBudgetProperties;
  MemoryBudgetProperties.pNext = nullptr;
  #ifdef SAGA_BUILD_IOS
  vkGetPhysicalDeviceMemoryProperties2KHR(PhysicalDevice, &MemoryProperties2);
  #else
  vkGetPhysicalDeviceMemoryProperties2(PhysicalDevice, &MemoryProperties2);
  #endif

  int heapIndex = 0;
  for (const auto& heap : MemoryProperties2.memoryProperties.memoryHeaps)
  {
    if (heap.flags & VkMemoryHeapFlagBits::VK_MEMORY_HEAP_DEVICE_LOCAL_BIT)
    {
      TotalMemory += heap.size;
      MemoryHeapIndices.push_back(heapIndex);
    }
    ++heapIndex;
  }
#endif // SAGA_EXTRA_API_MEMORY_INFO

  // Create framebuffer.
  if (ManageFramebuffer)
    createDefaultFramebuffer();
  createCommandBuffer();

  return true;
}

#ifdef SAGA_EXTRA_API_MEMORY_INFO
std::uint64_t CVulkanDriver::getAllocatedMemory() const
{
  std::uint64_t used = 0;
  MemoryBudgetProperties.pNext = nullptr;
  #ifdef SAGA_BUILD_IOS
  vkGetPhysicalDeviceMemoryProperties2KHR(PhysicalDevice, &MemoryProperties2);
  #else
  vkGetPhysicalDeviceMemoryProperties2(PhysicalDevice, &MemoryProperties2);
  #endif
  for (const auto heap : MemoryHeapIndices)
  {
    used += MemoryBudgetProperties.heapUsage[heap];
  }
  return used;
}
#endif // SAGA_EXTRA_API_MEMORY_INFO

void CVulkanDriver::createDefaultFramebuffer()
{
  // Free previous allocations.
  if (DefaultFramebuffer.handle)
  {
    vezDestroyFramebuffer(VKDevice, DefaultFramebuffer.handle);
    vezDestroyImageView(VKDevice, DefaultFramebuffer.colorImageView);
    vezDestroyImageView(VKDevice, DefaultFramebuffer.depthStencilImageView);
    vezDestroyImage(VKDevice, DefaultFramebuffer.colorImage);
    vezDestroyImage(VKDevice, DefaultFramebuffer.depthStencilImage);
  }

  // Get the current window dimension.
  auto width = Device.getWidth();
  auto height = Device.getHeight();

  // Get the swapchain's current surface format.
  VkSurfaceFormatKHR swapchainFormat = {};
  vezGetSwapchainSurfaceFormat(Swapchain, &swapchainFormat);

  // Create the color image for the Framebuffer.
  VezImageCreateInfo imageCreateInfo = {};
  imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
  imageCreateInfo.format = swapchainFormat.format;
  imageCreateInfo.extent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1 };
  imageCreateInfo.mipLevels = 1;
  imageCreateInfo.arrayLayers = 1;
  imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
  imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
  auto result = vezCreateImage(VKDevice, VEZ_MEMORY_GPU_ONLY, &imageCreateInfo, &DefaultFramebuffer.colorImage);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateImage failed %d", result);
    return;
  }

  // Create the image view for binding the texture as a resource.
  VezImageViewCreateInfo imageViewCreateInfo = {};
  imageViewCreateInfo.image = DefaultFramebuffer.colorImage;
  imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  imageViewCreateInfo.format = imageCreateInfo.format;
  imageViewCreateInfo.subresourceRange.layerCount = 1;
  imageViewCreateInfo.subresourceRange.levelCount = 1;
  result = vezCreateImageView(VKDevice, &imageViewCreateInfo, &DefaultFramebuffer.colorImageView);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateImageView failed %d", result);
    return;
  }

  // Create the depth image for the Framebuffer.
  imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
  imageCreateInfo.format = VK_FORMAT_D32_SFLOAT_S8_UINT;
  imageCreateInfo.extent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1 };
  imageCreateInfo.mipLevels = 1;
  imageCreateInfo.arrayLayers = 1;
  imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
  imageCreateInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
  result = vezCreateImage(VKDevice, VEZ_MEMORY_GPU_ONLY, &imageCreateInfo, &DefaultFramebuffer.depthStencilImage);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateImage failed %d", result);
    return;
  }

  // Create the image view for binding the texture as a resource.
  imageViewCreateInfo.image = DefaultFramebuffer.depthStencilImage;
  imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  imageViewCreateInfo.format = imageCreateInfo.format;
  imageViewCreateInfo.subresourceRange.layerCount = 1;
  imageViewCreateInfo.subresourceRange.levelCount = 1;
  result = vezCreateImageView(VKDevice, &imageViewCreateInfo, &DefaultFramebuffer.depthStencilImageView);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateImageView failed %d", result);
    return;
  }

  // Create the Framebuffer.
  std::array<VkImageView, 2> attachments = { DefaultFramebuffer.colorImageView, DefaultFramebuffer.depthStencilImageView };
  VezFramebufferCreateInfo framebufferCreateInfo = {};
  framebufferCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
  framebufferCreateInfo.pAttachments = attachments.data();
  framebufferCreateInfo.width = width;
  framebufferCreateInfo.height = height;
  framebufferCreateInfo.layers = 1;
  result = vezCreateFramebuffer(VKDevice, &framebufferCreateInfo, &DefaultFramebuffer.handle);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateFramebuffer failed %d", result);
    return;
  }
}

void CVulkanDriver::createCommandBuffer()
{
  // Get the graphics queue handle.
  vezGetDeviceGraphicsQueue(VKDevice, 0, &GraphicsQueue);

  // Create a command buffer handle.
  VezCommandBufferAllocateInfo allocInfo = {};
  allocInfo.queue = GraphicsQueue;
  allocInfo.commandBufferCount = 1;
  if (vezAllocateCommandBuffers(VKDevice, &allocInfo, &CommandBuffer) != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezAllocateCommandBuffers failed");
    exit(1);
  }
}

void CVulkanDriver::draw()
{
  if (CurrentPass == NULL_GPU_RESOURCE_HANDLE) return;
  const auto& renderPass = RenderPasses.at(CurrentPass);
  if (renderPass.DrawGeometry == false)
    return;

  auto& smgr = Device.getSceneManager();
  for (auto node : smgr->getNodeList(CurrentPass))
  {
    const auto& pipeline = Pipelines.at(node->getPipeline());
    auto& vulkanPipeline = VkPipelines[pipeline.Handle];
    // Bind the pipeline and associated resources.
    vezCmdBindPipeline(vulkanPipeline.Handle);

    // Set rasterization state.
    VezRasterizationState rasterizationState = {};
    rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizationState.depthClampEnable = pipeline.Rasterizer.DepthClamp;

    switch (pipeline.Rasterizer.CullMode)
    {
      case E_CULL_MODE::BACK_FACE: rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT; break;
      case E_CULL_MODE::FRONT_FACE: rasterizationState.cullMode = VK_CULL_MODE_FRONT_BIT; break;
      case E_CULL_MODE::FRONT_BACK_FACE: rasterizationState.cullMode = VK_CULL_MODE_FRONT_AND_BACK; break;
      case E_CULL_MODE::NONE: rasterizationState.cullMode = VK_CULL_MODE_NONE; break;
    }
    switch (pipeline.Rasterizer.FrontFaceMode)
    {
      case E_FRONT_FACE_MODE::CLOCKWISE: rasterizationState.frontFace = VK_FRONT_FACE_CLOCKWISE; break;
      case E_FRONT_FACE_MODE::COUNTER_CLOCKWISE: rasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE; break;
    }

    vezCmdSetRasterizationState(&rasterizationState);
    vezCmdSetDepthBias(pipeline.Rasterizer.DepthBias, pipeline.Rasterizer.DepthBiasClamp, pipeline.Rasterizer.DepthBiasSlope);

    // Set depth stencil state.
    VezDepthStencilState depthStencilState = {};
    depthStencilState.depthTestEnable = pipeline.DepthStencil.Enabled ? VK_TRUE : VK_FALSE;
    depthStencilState.depthWriteEnable = pipeline.DepthStencil.DepthWriteEnabled ? VK_TRUE : VK_FALSE;
    switch (pipeline.DepthStencil.DepthCompareFunc)
    {
      case E_COMPARE_FUNC::ALWAYS: depthStencilState.depthCompareOp = VK_COMPARE_OP_ALWAYS; break;
      case E_COMPARE_FUNC::EQUAL: depthStencilState.depthCompareOp = VK_COMPARE_OP_EQUAL; break;
      case E_COMPARE_FUNC::GREATER: depthStencilState.depthCompareOp = VK_COMPARE_OP_GREATER; break;
      case E_COMPARE_FUNC::GREATER_EQUAL: depthStencilState.depthCompareOp = VK_COMPARE_OP_GREATER_OR_EQUAL; break;
      case E_COMPARE_FUNC::LESS: depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS; break;
      case E_COMPARE_FUNC::LESS_EQUAL: depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL; break;
      case E_COMPARE_FUNC::NEVER: depthStencilState.depthCompareOp = VK_COMPARE_OP_NEVER; break;
      case E_COMPARE_FUNC::NOT_EQUAL: depthStencilState.depthCompareOp = VK_COMPARE_OP_NOT_EQUAL; break;
    }
    vezCmdSetDepthStencilState(&depthStencilState);

    if (pipeline.Blend.Enabled) {
      std::vector<VezColorBlendAttachmentState> blendAttachmentStates;
      for (int i = 0; i < renderPass.AttachmentCount; ++i) {
        VezColorBlendAttachmentState blendAttachmentState;
        const auto& state = pipeline.Blend.AttachmentStates[i];
        blendAttachmentState.blendEnable = state.Enabled ? VK_TRUE : VK_FALSE;
        blendAttachmentState.srcColorBlendFactor = vk::getBlendFactor(state.SrcColor);
        blendAttachmentState.dstColorBlendFactor = vk::getBlendFactor(state.DstColor);
        blendAttachmentState.colorBlendOp = vk::getBlendOp(state.ColorBlendOp);
        blendAttachmentState.srcAlphaBlendFactor = vk::getBlendFactor(state.SrcAlpha);
        blendAttachmentState.dstAlphaBlendFactor = vk::getBlendFactor(state.DstAlpha);
        blendAttachmentState.alphaBlendOp = vk::getBlendOp(state.AlphaBlendOp);
        blendAttachmentState.colorWriteMask = vk::getColorComponentFlags(state.ColorWriteMask);
        blendAttachmentStates.push_back(std::move(blendAttachmentState));
      }
      VezColorBlendState colorBlendState = {};
      colorBlendState.attachmentCount = pipeline.Blend.ColorAttachmentCount;
      colorBlendState.pAttachments = blendAttachmentStates.data();
      vezCmdSetColorBlendState(&colorBlendState);
    }
    vezCmdSetBlendConstants(pipeline.Blend.Factors);

    auto width = Device.getWidth();
    auto height = Device.getHeight();
    VkRect2D defaultScissor = { { 0, 0 },{ static_cast<uint32_t>(width), static_cast<uint32_t>(height) } };

    uint32_t i = 0;
    VkDeviceSize offsets[1] = { 0 };
    const auto& pass = RenderPasses.at(CurrentPass);

    auto n = std::dynamic_pointer_cast<scene::IMeshSceneNode>(node);
    const auto& mesh = n->getMesh();
    const auto& sceneNodeData = VkPipelineBuffers.at(node->getID()).at(pipeline.Handle);

    for (unsigned int i = 0; i < mesh->getMeshBufferCount(); ++i)
    {
      n->onRender();
      auto& meshBuffer = mesh->getMeshBuffer(i);

      if (meshBuffer.isNull())
      {
        vezCmdDraw(meshBuffer.getVertexCount(), meshBuffer.getInstanceCount(), 0, 0);
        continue;
      }
      if (meshBuffer.isGPUBuffer())
      {
        auto& buffer = *static_cast<scene::CGPUMeshBuffer*>(&meshBuffer);
        vezCmdBindVertexBuffers(meshBuffer.getBindingLocation(), 1, &VkShaderBuffers.at(buffer.getVertexBuffer()), offsets);
        if (buffer.getIndexCount() > 0)
          vezCmdBindIndexBuffer(VkShaderBuffers.at(buffer.getIndexBuffer()), 0, vk::getIndexType(buffer.getIndexType()));
      }
      else
      {
        vezCmdBindVertexBuffers(meshBuffer.getBindingLocation(), 1, &sceneNodeData.VertexBuffer, offsets);
        vezCmdBindIndexBuffer(sceneNodeData.IndexBuffer, 0, vk::getIndexType(meshBuffer.getIndexType()));
      }

      VezInputAssemblyState inputState = {};
      inputState.topology = saga::video::vk::getTopology(meshBuffer.getPrimitiveType());
      vezCmdSetInputAssemblyState(&inputState);
      vezCmdSetVertexInputFormat(vulkanPipeline.VertexInputFormat);

      if (n->hasDrawCommands()) {
        for (const auto& cmd : n->getDrawCommands()) {
          if (cmd.UseScissor) {
            const auto& rect = cmd.Scissor;
            VkRect2D clipRect = {
              { static_cast<std::int32_t>(rect.x), static_cast<std::int32_t>(rect.y) },
              { static_cast<std::uint32_t>(rect.z), static_cast<std::uint32_t>(rect.w) }
            };
            vezCmdSetScissor(0, 1, &clipRect);
          } else {
            vezCmdSetScissor(0, 1, &defaultScissor);
          }
          if (cmd.IndexedDraw) {
            vezCmdDrawIndexed(cmd.IndexCount, cmd.InstanceCount, cmd.IndexOffset, cmd.VertexOffset, cmd.FirstInstance);
          } else {
            vezCmdDraw(cmd.VertexCount, cmd.InstanceCount, cmd.VertexOffset, cmd.FirstInstance);
          }
        }
        continue;
      }

      if (meshBuffer.useIndirectDraw())
      {
        const auto& cmdInfo = IndirectBuffers.at(meshBuffer.indirectDrawBuffer());
        const auto count = cmdInfo.Count;
        const auto stride = sizeof(SIndirectCommand);
        const auto buffer = VkIndirectBuffers.at(cmdInfo.Handle);
        vezCmdDrawIndirect(buffer, 0, count, stride);
      }
      else if (meshBuffer.useIndexedIndirectDraw())
      {
        const auto& cmdInfo = IndexedIndirectBuffers.at(meshBuffer.indexedIndirectDrawBuffer());
        const auto count = cmdInfo.Count;
        const auto stride = sizeof(SIndexedIndirectCommand);
        const auto buffer = VkIndexedIndirectBuffers.at(cmdInfo.Handle);
        vezCmdDrawIndexedIndirect(buffer, 0, count, stride);
      }
      else if (meshBuffer.getIndexCount() > 0)
      {
        vezCmdDrawIndexed(meshBuffer.getIndexCount(), meshBuffer.getInstanceCount(), 0, 0, 0);
      }
      else
      {
        vezCmdDraw(meshBuffer.getVertexCount(), meshBuffer.getInstanceCount(), 0, 0);
      }
    }
  }
}

bool CVulkanDriver::hasPipelineBuffer(const scene::IMeshSceneNode& node, const video::PipelineHandle pipeline) const {
  if (VkPipelineBuffers.count(node.getID()) == 0) return false;
  return VkPipelineBuffers.at(node.getID()).count(pipeline) > 0;
}


void CVulkanDriver::createPipelineBuffer(const scene::IMeshSceneNode& node, const video::PipelineHandle pipeline)
{
  auto& meshBuffer = node.getMesh()->getMeshBuffer();
  if (VkPipelineBuffers[node.getID()].count(pipeline)) return;
  if (meshBuffer.isNull())
  {
    VkPipelineBuffers[node.getID()][pipeline] = { VK_NULL_HANDLE, VK_NULL_HANDLE };
    return;
  }

  if (meshBuffer.isGPUBuffer())
  {
    auto& buffer = *static_cast<scene::CGPUMeshBuffer*>(&meshBuffer);
    VkPipelineBuffers[node.getID()][pipeline] = { VkShaderBuffers.at(buffer.getVertexBuffer()), VK_NULL_HANDLE };
    return;
  }

  VkBuffer vertexBuffer;
  VezBufferCreateInfo bufferCreateInfo = {};
  bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  bufferCreateInfo.size = meshBuffer.getSize(pipeline);
  auto result = vezCreateBuffer(VKDevice, VEZ_MEMORY_GPU_ONLY, &bufferCreateInfo, &vertexBuffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateBuffer for vertex buffer error");
    exit(1);
  }

  result = vezBufferSubData(VKDevice, vertexBuffer, 0, bufferCreateInfo.size, meshBuffer.getData(pipeline));

  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezBufferSubData for vertex buffer error");
    exit(1);
  }

  VkBuffer indexBuffer;
  bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
  bufferCreateInfo.size = sizeof(std::uint32_t) * meshBuffer.getIndexCount();
  result = vezCreateBuffer(VKDevice, VEZ_MEMORY_GPU_ONLY, &bufferCreateInfo, &indexBuffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateBuffer for index buffer error");
    exit(1);
  }

  result = vezBufferSubData(VKDevice, indexBuffer, 0, bufferCreateInfo.size, static_cast<const void*>(meshBuffer.getIndices()));
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezBufferSubData for index buffer error");
    exit(1);
  }

  VkPipelineBuffers[node.getID()][pipeline] = { vertexBuffer, indexBuffer };
}

void CVulkanDriver::destroyPipelineBuffer(const scene::ISceneNode& node, bool destroyAll)
{
  PipelineBuffers buffers;
  if (destroyAll)
  {
    buffers = VkPipelineBuffers.at(node.getID());
  }
  else
  {
    auto buffer = VkPipelineBuffers.at(node.getID()).at(node.getPipeline());
    buffers.insert({ node.getPipeline(), buffer });
  }

  for (auto buffer : buffers)
  {
    vezDestroyBuffer(VKDevice, buffer.second.VertexBuffer);
    vezDestroyBuffer(VKDevice, buffer.second.IndexBuffer);
    VkPipelineBuffers[node.getID()].erase(buffer.first);
  }
}

void CVulkanDriver::createFramebuffer(const RenderPassHandle pass)
{
  auto& renderPass = RenderPasses[pass];
  VezFramebuffer framebuffer;

  std::vector<VkImageView> attachments;
  for (auto& att : renderPass.ColorAttachments)
  {
    if (att != NULL_GPU_RESOURCE_HANDLE)
      attachments.push_back(VkTextures.at(att).ImageView);
  }
  if (renderPass.DepthStencilAttachment != NULL_GPU_RESOURCE_HANDLE)
    attachments.push_back(VkTextures.at(renderPass.DepthStencilAttachment).ImageView);

  if (attachments.empty())
    return;

  VezFramebufferCreateInfo framebufferCreateInfo = {};
  framebufferCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
  framebufferCreateInfo.pAttachments = attachments.data();
  framebufferCreateInfo.width = renderPass.Width == 0 ? getWidth() : renderPass.Width;
  framebufferCreateInfo.height = renderPass.Height == 0 ? getHeight() : renderPass.Height;
  framebufferCreateInfo.layers = 1;
  auto result = vezCreateFramebuffer(VKDevice, &framebufferCreateInfo, &framebuffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateFramebuffer failed %d", result);
  }
  else
  {
    VkFramebuffers[pass] = framebuffer;
  }
}

void CVulkanDriver::destroyFrameBuffer(const RenderPassHandle pass)
{
  vezDestroyFramebuffer(VKDevice, VkFramebuffers[pass]);
  VkFramebuffers.erase(pass);
}

RenderPassHandle CVulkanDriver::createResource(SRenderPass&& pass)
{
  auto handle = CVideoDriver::createResource(std::move(pass));
  Pipelines[handle] = {};
  auto& renderPass = RenderPasses[handle];
  renderPass.UpdateAttachments = true;
  return handle;
}

PipelineHandle CVulkanDriver::createResource(SPipeline&& pipelineInfo)
{
  auto handle = CVideoDriver::createResource(std::move(pipelineInfo));
  auto& pipeline = Pipelines[handle];
  pipeline.isCompute = Shaders.at(pipeline.Shaders).CSource.empty() ? false : true;
  auto& shaderModules = VkShaderModules[pipeline.Shaders];
  std::vector<VezPipelineShaderStageCreateInfo> shaderStageCreateInfo;
  for (const auto& shaderModule : shaderModules)
  {
    VezPipelineShaderStageCreateInfo shaderStage;
    shaderStage.module = shaderModule;
    shaderStage.pEntryPoint = "main";
    shaderStage.pSpecializationInfo = nullptr;
    shaderStageCreateInfo.push_back(shaderStage);
  }

  VulkanPipeline vulkanPipeline;
  if (pipeline.isCompute)
  {
    VezComputePipelineCreateInfo pipelineCreateInfo = {};
    pipelineCreateInfo.pStage = shaderStageCreateInfo.data();
    if (vezCreateComputePipeline(VKDevice, &pipelineCreateInfo, &vulkanPipeline.Handle) != VK_SUCCESS)
    {
      SDL_LogError(SDL_LOG_CATEGORY_ERROR, "vkCreateGraphicsPipeline failed");
      return false;
    }
  }
  else
  {
    VezGraphicsPipelineCreateInfo pipelineCreateInfo = {};
    pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStageCreateInfo.size());
    pipelineCreateInfo.pStages = shaderStageCreateInfo.data();
    if (vezCreateGraphicsPipeline(VKDevice, &pipelineCreateInfo, &vulkanPipeline.Handle) != VK_SUCCESS)
    {
      SDL_LogError(SDL_LOG_CATEGORY_ERROR, "vkCreateGraphicsPipeline failed");
      return false;
    }

    std::uint32_t vertexInputSize = 0;
    std::vector<VkVertexInputAttributeDescription> attribDesc;
    auto& attributes = pipeline.Layout.Attributes;
    auto getOffset = [&attributes] (int binding, int index) {
      uint32_t offset = 0;
      int i = 0;
      for (const auto& attr : attributes.at(binding))
      {
        if (attr.Type != E_ATTRIBUTE_TYPE::INVALID)
        {
          if (i++ < index)
            offset += GetAttributeSize(attr.Format);
        }
      }
      return offset;
    };
    uint32_t binding = 0;
    uint32_t location = 0;
    for (const auto& attributeArray : pipeline.Layout.Attributes)
    {
      int index = 0;
      for (const auto& attr : attributeArray)
      {
        if (attr.Type != E_ATTRIBUTE_TYPE::INVALID)
        {
          vertexInputSize += GetAttributeSize(attr.Format);
          attribDesc.push_back(
            { location++, binding, saga::video::vk::getFormat(attr.Format), getOffset(binding, index++) }
          );
        }
      }
      ++binding;
    }

    vulkanPipeline.VertexInputBinding = {
      0, vertexInputSize, VK_VERTEX_INPUT_RATE_VERTEX
    };

    VezVertexInputFormatCreateInfo vertexInputFormatCreateInfo = {};
    vertexInputFormatCreateInfo.vertexBindingDescriptionCount = pipeline.VertexBindingCount;
    vertexInputFormatCreateInfo.pVertexBindingDescriptions = &vulkanPipeline.VertexInputBinding;
    vertexInputFormatCreateInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attribDesc.size());
    vertexInputFormatCreateInfo.pVertexAttributeDescriptions = attribDesc.data();
    auto result = vezCreateVertexInputFormat(VKDevice, &vertexInputFormatCreateInfo, &vulkanPipeline.VertexInputFormat);
  }

  VkPipelines[handle] = vulkanPipeline;
  return handle;
}

VkShaderModule CVulkanDriver::createShader(ShaderHandle shader, VkShaderStageFlagBits stage)
{
  auto& shaders = Shaders[shader];
  const char* code = nullptr;
  std::size_t codeSize = 0;
  switch (stage)
  {
    case VK_SHADER_STAGE_VERTEX_BIT:
    {
      code = shaders.VSSource.c_str();
      codeSize = shaders.VSSource.size();
    } break;

    case VK_SHADER_STAGE_GEOMETRY_BIT:
    {
      code = shaders.GSSource.c_str();
      codeSize = shaders.GSSource.size();
    } break;

    case VK_SHADER_STAGE_FRAGMENT_BIT:
    {
      code = shaders.FSSource.c_str();
      codeSize = shaders.FSSource.size();
    } break;

    case VK_SHADER_STAGE_COMPUTE_BIT:
    {
      code = shaders.CSource.c_str();
      codeSize = shaders.CSource.size();
    } break;
  }

  VezShaderModuleCreateInfo createInfo = {};
  createInfo.stage = stage;
  createInfo.pGLSLSource = code;
  createInfo.codeSize = codeSize;
  createInfo.pEntryPoint = "main";

  VkShaderModule shaderModule = VK_NULL_HANDLE;
  auto result = vezCreateShaderModule(VKDevice, &createInfo, &shaderModule);
  if (result != VK_SUCCESS && shaderModule != VK_NULL_HANDLE)
  {
    // If shader module creation failed but error is from GLSL compilation, get the error log.
    uint32_t infoLogSize = 0;
    vezGetShaderModuleInfoLog(shaderModule, &infoLogSize, nullptr);

    std::string infoLog(infoLogSize, '\0');
    vezGetShaderModuleInfoLog(shaderModule, &infoLogSize, &infoLog[0]);
    vezDestroyShaderModule(VKDevice, shaderModule);

    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Failed to create shader");
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, infoLog.c_str());
    exit(-1);
  }
  return shaderModule;
}

ShaderUniformHandle CVulkanDriver::createResource(SShaderUniform&& uniform)
{
  auto handle = CVideoDriver::createResource(std::move(uniform));
  const auto& uniformInfo = ShaderUniforms.at(handle);
  VkBuffer ubo;

  VezBufferCreateInfo bufferCreateInfo = {};
  bufferCreateInfo.size = uniformInfo.Size;
  bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
  auto result = vezCreateBuffer(VKDevice, VEZ_MEMORY_CPU_TO_GPU, &bufferCreateInfo, &ubo);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateBuffer for shader uniform error");
    exit(1);
  }
  VkShaderUniforms[handle] = ubo;
  return handle;
}

ShaderBufferHandle CVulkanDriver::createResource(SShaderBuffer&& buffer)
{
  auto handle = CVideoDriver::createResource(std::move(buffer));
  const auto& bufferInfo = ShaderBuffers.at(handle);
  VkBuffer shaderBuffer;

  VezBufferCreateInfo bufferCreateInfo = {};
  bufferCreateInfo.size = bufferInfo.Size;
  bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

  auto memoryFlag = bufferInfo.ReadBack == false ? VEZ_MEMORY_GPU_ONLY : VEZ_MEMORY_GPU_TO_CPU;
  if (bufferInfo.VertexBufferBind)
    bufferCreateInfo.usage |= VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  if (bufferInfo.IndexBufferBind)
    bufferCreateInfo.usage |= VK_BUFFER_USAGE_INDEX_BUFFER_BIT;

  auto result = vezCreateBuffer(VKDevice, memoryFlag, &bufferCreateInfo, &shaderBuffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateBuffer for shader buffer error");
    exit(1);
  }
  VkShaderBuffers[handle] = shaderBuffer;
  return handle;
}

IndirectBufferHandle CVulkanDriver::createResource(SIndirectBuffer&& buffer)
{
  auto handle = CVideoDriver::createResource(std::move(buffer));
  const auto& bufferInfo = IndirectBuffers.at(handle);
  VkBuffer indirectBuffer;

  VezBufferCreateInfo bufferCreateInfo = {};
  bufferCreateInfo.size = bufferInfo.Count * sizeof(SIndirectCommand);
  bufferCreateInfo.usage = VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

  if (bufferInfo.ShaderBinding)
    bufferCreateInfo.usage |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

  auto result = vezCreateBuffer(VKDevice, VEZ_MEMORY_GPU_ONLY, &bufferCreateInfo, &indirectBuffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateBuffer for shader buffer error");
    exit(1);
  }
  vezBufferSubData(VKDevice, indirectBuffer, 0, bufferCreateInfo.size, bufferInfo.Commands.data());
  VkIndirectBuffers[handle] = indirectBuffer;
  return handle;
}

IndexedIndirectBufferHandle CVulkanDriver::createResource(SIndexedIndirectBuffer&& buffer)
{
  auto handle = CVideoDriver::createResource(std::move(buffer));
  const auto& bufferInfo = IndexedIndirectBuffers.at(handle);
  VkBuffer indexedIndirectBuffer;

  VezBufferCreateInfo bufferCreateInfo = {};
  bufferCreateInfo.size = bufferInfo.Count * sizeof(SIndexedIndirectCommand);
  bufferCreateInfo.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

  if (bufferInfo.ShaderBinding)
    bufferCreateInfo.usage |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

  auto result = vezCreateBuffer(VKDevice, VEZ_MEMORY_GPU_ONLY, &bufferCreateInfo, &indexedIndirectBuffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateBuffer for shader buffer error");
    exit(1);
  }
  vezBufferSubData(VKDevice, indexedIndirectBuffer, 0, bufferCreateInfo.size, bufferInfo.Commands.data());
  VkIndexedIndirectBuffers[handle] = indexedIndirectBuffer;
  return handle;
}

void CVulkanDriver::updateShaderUniform(const ShaderUniformHandle uniformHandle, const void* data)
{
  void* buffer = nullptr;
  auto& ubo = VkShaderUniforms[uniformHandle];
  auto& uniform = ShaderUniforms[uniformHandle];
  auto result = vezBufferSubData(VKDevice, ubo, 0, uniform.Size, data);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkMapBuffer for uniform failed");
    exit(1);
  }
}

void CVulkanDriver::updateShaderBuffer(const ShaderBufferHandle buffer, const void* data, const size_t offset, const size_t size)
{
  auto& shaderBuffer = VkShaderBuffers[buffer];
  auto result = vezBufferSubData(VKDevice, shaderBuffer, offset, size > 0 ? size : ShaderBuffers.at(buffer).Size, data);
  if (result != VK_SUCCESS)
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkBufferSubData failed for shader buffer");
}

void CVulkanDriver::updatePushConstant(const PushConstantHandle con, const void* data, const size_t offset,  const size_t size)
{
  auto& pushConstant = PushConstants[con];
  vezCmdPushConstants(offset, size > 0 ? size : pushConstant.Size, data);
}

void CVulkanDriver::bindTexture(TextureHandle texture, const int binding)
{
  const auto& vkTexture = VkTextures.at(texture);
  vezCmdBindImageView(vkTexture.ImageView, vkTexture.Sampler, 0, binding, 0);
}

void CVulkanDriver::bindShaderUniform(const ShaderUniformHandle uniform, const int binding)
{
  auto& ubo = VkShaderUniforms[uniform];
  vezCmdBindBuffer(ubo, 0, VK_WHOLE_SIZE, 0, binding, 0);
}

void CVulkanDriver::bindShaderBuffer(const ShaderBufferHandle buffer, const int binding)
{
  auto& shaderBuffer = VkShaderBuffers[buffer];
  vezCmdBindBuffer(shaderBuffer, 0, VK_WHOLE_SIZE, 0, binding, 0);
}

ShaderHandle CVulkanDriver::createResource(SShader&& shader)
{
  auto handle = CVideoDriver::createResource(std::move(shader));

  auto& shaderSource = Shaders[handle];
  auto& shaderModules = VkShaderModules[handle];

  if (shaderSource.VSSource.empty() == false)
    shaderModules.push_back(createShader(handle, VK_SHADER_STAGE_VERTEX_BIT));
  if (shaderSource.GSSource.empty() == false)
    shaderModules.push_back(createShader(handle, VK_SHADER_STAGE_GEOMETRY_BIT));
  if (shaderSource.FSSource.empty() == false)
    shaderModules.push_back(createShader(handle, VK_SHADER_STAGE_FRAGMENT_BIT));
  if (shaderSource.CSource.empty() == false)
    shaderModules.push_back(createShader(handle, VK_SHADER_STAGE_COMPUTE_BIT));

  return handle;
}

TextureHandle CVulkanDriver::createTexture(STexture&& texture)
{
  auto handle = CVideoDriver::createTexture(std::move(texture));
  CVulkanDriver::createTexture(handle);
  return handle;
}

TextureHandle CVulkanDriver::createTexture(const std::string& path, const E_PIXEL_FORMAT format)
{
  auto handle = CVideoDriver::createTexture(path, format);
  CVulkanDriver::createTexture(handle);
  return handle;
}

TextureHandle CVulkanDriver::createTexture(unsigned char* fileData, const std::size_t size, const E_PIXEL_FORMAT format)
{
  auto handle = CVideoDriver::createTexture(fileData, size, format);
  CVulkanDriver::createTexture(handle);
  return handle;
}

TextureHandle CVulkanDriver::createTexture(unsigned char* pixelData, const int width, const int height, const E_PIXEL_FORMAT format)
{
  auto handle = CVideoDriver::createTexture(pixelData, width, height, format);
  CVulkanDriver::createTexture(handle);
  return handle;
}

void CVulkanDriver::createTexture(TextureHandle textureHandle)
{
  auto& texture = Textures[textureHandle];
  VulkanTexture vkTexture;

  int arrayLayers = 0;
  if (texture.IsDepthAttachment || texture.IsRenderTarget)
  {
    arrayLayers = 1;
  }
  else
  {
    for (int i = 0; i < MAX_IMAGE_LAYERS; ++i)
    {
      if (texture.getData(i, 0) != nullptr)
      {
        ++arrayLayers;
      }
    }
  }

  VezImageCreateInfo imageCreateInfo = {};
  imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
  imageCreateInfo.format = saga::video::vk::getFormat(texture.Format);
  imageCreateInfo.extent = { static_cast<uint32_t>(texture.Width), static_cast<uint32_t>(texture.Height), 1 };
  imageCreateInfo.mipLevels = texture.MipLevels;
  imageCreateInfo.arrayLayers = arrayLayers;
  imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;

  if (texture.Type == E_TEXTURE_TYPE::CUBE_MAP)
  {
    imageCreateInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
  }

  if (texture.IsRenderTarget)
  {
    const auto usageFlag = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    if (texture.IsDepthAttachment)
    {
      imageCreateInfo.usage = usageFlag | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    }
    else
    {
      imageCreateInfo.usage = usageFlag | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    }
  }
  else
  {
    imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
  }

  auto result = vezCreateImage(VKDevice, VEZ_MEMORY_GPU_ONLY, &imageCreateInfo, &vkTexture.Image);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateImage failed");
  }

  if (texture.IsRenderTarget == false)
  {
    for (int face = 0; face < 6; ++face)
    {
      for (int level = 0; level < texture.MipLevels; ++level)
      {
        const auto& content = texture.Contents[face][level];
        if (content.Data == nullptr) continue;
        VezImageSubDataInfo subDataInfo = {};
        subDataInfo.imageSubresource.baseArrayLayer = face;
        subDataInfo.imageSubresource.mipLevel = level;
        subDataInfo.imageSubresource.layerCount = 1;
        subDataInfo.imageOffset = { 0, 0, 0 };
        subDataInfo.imageExtent = { imageCreateInfo.extent.width >> level, imageCreateInfo.extent.height >> level, 1 };
        result = vezImageSubData(VKDevice, vkTexture.Image, &subDataInfo, content.Data);
        if (result != VK_SUCCESS)
        {
          SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezImageSubData failed");
        }
        if (!texture.KeepData) texture.destroyData(face, level);
      }
    }
  }

  VkImageViewType viewType;
  switch (texture.Type)
  {
    case E_TEXTURE_TYPE::TEXTURE_2D:
    {
      viewType = VK_IMAGE_VIEW_TYPE_2D;
    } break;

    case E_TEXTURE_TYPE::CUBE_MAP:
    {
      viewType = VK_IMAGE_VIEW_TYPE_CUBE;
    } break;

    case E_TEXTURE_TYPE::TEXTURE_2D_ARRAY:
    {
      viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
    } break;
  }

  VezImageViewCreateInfo imageViewCreateInfo = {};
  imageViewCreateInfo.image = vkTexture.Image;
  imageViewCreateInfo.viewType = viewType;
  imageViewCreateInfo.format = imageCreateInfo.format;
  imageViewCreateInfo.subresourceRange.layerCount = arrayLayers;
  imageViewCreateInfo.subresourceRange.levelCount = imageCreateInfo.mipLevels;
  result = vezCreateImageView(VKDevice, &imageViewCreateInfo, &vkTexture.ImageView);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateImageView failed");
  }

  VezSamplerCreateInfo samplerCreateInfo = {};
  samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
  samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
  samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
  samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  samplerCreateInfo.minLod = texture.MinLOD;
  samplerCreateInfo.maxLod = texture.MaxLOD;
  result = vezCreateSampler(VKDevice, &samplerCreateInfo, &vkTexture.Sampler);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateSampler failed");
  } else
  {
    VkTextures[textureHandle] = vkTexture;
  }

  if (!texture.GenerateMipmap) return;

  CVulkanDriver::begin();

  auto width = texture.Width;
  auto height = texture.Height;
  for (auto level = 1; level < texture.MipLevels; ++level)
  {
    VezImageBlit blitInfo = {};
    blitInfo.srcSubresource.mipLevel = level - 1;
    blitInfo.srcSubresource.baseArrayLayer = 0;
    blitInfo.srcSubresource.layerCount = 1;
    blitInfo.srcOffsets[1].x = texture.Width >> (level - 1);
    blitInfo.srcOffsets[1].y = texture.Height >> (level - 1);
    blitInfo.srcOffsets[1].z = 1;

    blitInfo.dstSubresource.mipLevel = level;
    blitInfo.dstSubresource.baseArrayLayer = 0;
    blitInfo.dstSubresource.layerCount = 1;
    blitInfo.dstOffsets[1].x = texture.Width >> level;
    blitInfo.dstOffsets[1].y = texture.Height >> level;
    blitInfo.dstOffsets[1].z = 1;

    // TODO: more mipmap mode
    vezCmdBlitImage(vkTexture.Image, vkTexture.Image, 1, &blitInfo, VK_FILTER_LINEAR);
  }

  CVulkanDriver::end();
  CVulkanDriver::submit();
}

void CVulkanDriver::bindComputePipeline(const PipelineHandle& compute)
{
  vezCmdBindPipeline(VkPipelines.at(compute).Handle);
}

void CVulkanDriver::dispatchComputePipeline(std::uint32_t x, std::uint32_t y, uint32_t z)
{
  vezCmdDispatch(x, y, z);
}

void CVulkanDriver::begin()
{
  vkDeviceWaitIdle(VKDevice);
  vezResetCommandBuffer(CommandBuffer);

  // Begin command buffer recording.
  auto result  = vezBeginCommandBuffer(CommandBuffer, VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezBeginCommandBuffer failed");
    exit(1);
  }
}

void CVulkanDriver::beginPass(RenderPassHandle handle)
{
  CVideoDriver::beginPass(handle);

  auto& pass = RenderPasses[CurrentPass];

  int width = pass.Width;
  int height = pass.Height;

  if (width == 0)
    width = Device.getWidth();
  if (height == 0)
    height = Device.getHeight();

  VkRect2D defaultScissor = { { 0, 0 },{ static_cast<uint32_t>(width), static_cast<uint32_t>(height) } };

  const auto& clear = pass.State.Colors[0].Values;

  VkClearColorValue clearColor = {
    { clear.x, clear.y, clear.z, clear.w }
  };

  glm::vec4 defaultViewport = { 0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height) };
  VkViewport defaultVkViewport = { defaultViewport.x, defaultViewport.y, defaultViewport.z, defaultViewport.w, 0.0f, 1.0f };
  vezCmdSetViewport(0, 1, &defaultVkViewport);
  vezCmdSetScissor(0, 1, &defaultScissor);

  vezCmdSetViewportState(1);

  // Define clear values for the swapchain's color and depth attachments.
  std::vector<VezAttachmentReference> attachmentReferences;

  if (!pass.UseDefaultAttachments && pass.UpdateAttachments)
  {
    if (VkFramebuffers.count(handle))
    {
      destroyFrameBuffer(handle);
    }
    createFramebuffer(handle);
    pass.UpdateAttachments = false;
  }

  if (pass.UseDefaultAttachments)
  {
    attachmentReferences.resize(2);
    attachmentReferences[0].clearValue.color = clearColor;
    attachmentReferences[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachmentReferences[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachmentReferences[1].clearValue.depthStencil.depth = pass.State.Depth.Value;
    attachmentReferences[1].clearValue.depthStencil.stencil = pass.State.Stencil.Value;
    attachmentReferences[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachmentReferences[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  }
  else
  {
    int i = 0;
    for (const auto& att : pass.ColorAttachments)
    {
      if (att != NULL_GPU_RESOURCE_HANDLE)
      {
        const auto& state = pass.State.Colors.at(i).State;
        const auto& color = pass.State.Colors.at(i).Values;
        VezAttachmentReference ref;
        switch (state)
        {
          case E_ATTACHMENT_STATE::CLEAR: ref.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR; break;
          case E_ATTACHMENT_STATE::LOAD: ref.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD; break;
        }
        ref.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        ref.clearValue.color = { color.x, color.y, color.z, color.w };
        attachmentReferences.push_back(std::move(ref));
      }
      ++i;
    }
    if (pass.DepthStencilAttachment != NULL_GPU_RESOURCE_HANDLE)
    {
      VezAttachmentReference ref;
      switch (pass.State.Depth.State)
      {
        case E_ATTACHMENT_STATE::CLEAR: ref.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR; break;
        case E_ATTACHMENT_STATE::LOAD: ref.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD; break;
      }
      ref.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      ref.clearValue.depthStencil.depth = pass.State.Depth.Value;
      ref.clearValue.depthStencil.stencil = pass.State.Stencil.Value;
      attachmentReferences.push_back(std::move(ref));
    }
  }

  pass.AttachmentCount = attachmentReferences.size();

  VezImageSubresourceRange range = {};
  range.layerCount = 1;
  range.levelCount = 1;

  auto smgr = std::dynamic_pointer_cast<scene::CSceneManager>(Device.getSceneManager());

  if (smgr->isEmptyPass(pass.Handle))
  {
    vezCmdClearColorImage(DefaultFramebuffer.colorImage, &clearColor, 1, &range);
    return;
  }

  // Begin a render pass.
  VezRenderPassBeginInfo beginInfo = {};
  beginInfo.framebuffer = pass.UseDefaultAttachments ? DefaultFramebuffer.handle : VkFramebuffers[pass.Handle];
  beginInfo.attachmentCount = static_cast<uint32_t>(attachmentReferences.size());
  beginInfo.pAttachments = attachmentReferences.data();
  vezCmdBeginRenderPass(&beginInfo);
}

void CVulkanDriver::end()
{
  // End command buffer recording.
  if (vezEndCommandBuffer() != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezEndCommandBuffer failed");
    exit(1);
  }
}

void CVulkanDriver::endPass()
{
  // End the render pass.
  if (CurrentPass != NULL_GPU_RESOURCE_HANDLE) {
    if (RenderPasses.at(CurrentPass).DrawGeometry && !SceneManager->isEmptyPass(CurrentPass))
      vezCmdEndRenderPass();
  }

  CVideoDriver::endPass();
}


void CVulkanDriver::destroyTexture(const TextureHandle texture)
{
  auto& vkTexture = VkTextures[texture];
  vezDestroySampler(VKDevice, vkTexture.Sampler);
  vezDestroyImageView(VKDevice, vkTexture.ImageView);
  vezDestroyImage(VKDevice, vkTexture.Image);
  VkTextures.erase(texture);
  CVideoDriver::destroyTexture(texture);
}

void CVulkanDriver::destroyShader(const ShaderHandle shader)
{
  for (auto& module : VkShaderModules[shader])
  {
    vezDestroyShaderModule(VKDevice, module);
  }
  VkShaderModules.erase(shader);
  CVideoDriver::destroyShader(shader);
}

void CVulkanDriver::destroyShaderUniform(const ShaderUniformHandle uniform)
{
  vezDestroyBuffer(VKDevice, VkShaderUniforms.at(uniform));
  VkShaderUniforms.erase(uniform);
  CVideoDriver::destroyShaderUniform(uniform);
}

void CVulkanDriver::destroyShaderBuffer(const ShaderBufferHandle buffer)
{
  vezDestroyBuffer(VKDevice, VkShaderBuffers.at(buffer));
  VkShaderBuffers.erase(buffer);
  CVideoDriver::destroyShaderBuffer(buffer);
}

void CVulkanDriver::destroyIndirectBuffer(const IndirectBufferHandle buffer)
{
  vezDestroyBuffer(VKDevice, VkIndirectBuffers.at(buffer));
  VkIndirectBuffers.erase(buffer);
  CVideoDriver::destroyIndirectBuffer(buffer);
}

void CVulkanDriver::destroyIndexedIndirectBuffer(const IndexedIndirectBufferHandle buffer)
{
  vezDestroyBuffer(VKDevice, VkIndexedIndirectBuffers.at(buffer));
  VkIndexedIndirectBuffers.erase(buffer);
  CVideoDriver::destroyIndexedIndirectBuffer(buffer);
}

void* CVulkanDriver::mapBuffer(const ShaderBufferHandle buffer, const std::size_t offset, const std::size_t size) const
{
  void* data = nullptr;
  vezMapBuffer(VKDevice, VkShaderBuffers.at(buffer), offset, size > 0 ? size : ShaderBuffers.at(buffer).Size, &data);
  return data;
}

void CVulkanDriver::unmapBuffer(const ShaderBufferHandle buffer) const
{
  vezUnmapBuffer(VKDevice, VkShaderBuffers.at(buffer));
}

void CVulkanDriver::destroyPipeline(const PipelineHandle p)
{
  for (auto& passIt : SceneManager->getNodeList())
  {
    for (auto& node : passIt.second)
    {
      if (node->getPipeline() == p)
      {
        destroyPipelineBuffer(*node);
        node->setPipeline(video::NULL_GPU_RESOURCE_HANDLE);
      }
    }
  }

  vezDestroyPipeline(VKDevice, VkPipelines[p].Handle);
  VkPipelines.erase(p);
  CVideoDriver::destroyPipeline(p);
}

void CVulkanDriver::destroyRenderPass(const RenderPassHandle pass)
{
  destroyFrameBuffer(pass);
  CVideoDriver::destroyRenderPass(pass);
}

void CVulkanDriver::submit()
{
  // Submit the command buffer to the graphics queue.
  VezSubmitInfo submitInfo = {};
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &CommandBuffer;

  // Request a wait semaphore to pass to present so it waits for rendering to complete.
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &Semaphore;
  if (vezQueueSubmit(GraphicsQueue, 1, &submitInfo, nullptr)!= VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezQueueSubmit failed");
    exit(1);
  }
  vezDeviceWaitIdle(VKDevice);
}

void CVulkanDriver::present(TextureHandle texture)
{
  vkDeviceWaitIdle(VKDevice);

  if (CreationParams.DriverType == video::E_DRIVER_TYPE::VULKAN_HEADLESS) return;

  // Present the swapchain framebuffer to the window.
  VkPipelineStageFlags waitDstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

  VezPresentInfo presentInfo = {};
  presentInfo.waitSemaphoreCount = 1;
  presentInfo.pWaitSemaphores = &Semaphore;
  presentInfo.pWaitDstStageMask = &waitDstStageMask;
  presentInfo.swapchainCount = 1;
  presentInfo.pSwapchains = &Swapchain;
  presentInfo.pImages = {
    texture == NULL_GPU_RESOURCE_HANDLE ?
    &DefaultFramebuffer.colorImage :
    &VkTextures[texture].Image
  };

  if (vezQueuePresent(GraphicsQueue, &presentInfo) != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezQueuePresentKHR failed");
    exit(1);
  }
  vkDeviceWaitIdle(VKDevice);
}

void CVulkanDriver::copyTexture(
  TextureHandle srcTex, TextureHandle dstTex,
  const glm::ivec2& srcOffset, const glm::ivec2& dstOffset, const glm::ivec2& size)
{
  VezImageCopy copyInfo = {};
  copyInfo.srcSubresource.mipLevel = 0;
  copyInfo.srcSubresource.baseArrayLayer = 0;
  copyInfo.srcSubresource.layerCount = 1;
  copyInfo.srcOffset.x = srcOffset.x;
  copyInfo.srcOffset.y = srcOffset.y;
  copyInfo.srcOffset.z = 0;

  copyInfo.dstSubresource.mipLevel = 0;
  copyInfo.dstSubresource.baseArrayLayer = 0;
  copyInfo.dstSubresource.layerCount = 1;
  copyInfo.dstOffset.x = dstOffset.x;
  copyInfo.dstOffset.y = dstOffset.y;
  copyInfo.dstOffset.z = 0;
  copyInfo.extent.width = size.x != 0 ? size.x : Textures.at(srcTex).Width;
  copyInfo.extent.height = size.y != 0 ? size.y : Textures.at(srcTex).Height;
  copyInfo.extent.depth = 1;

  vezCmdCopyImage(VkTextures.at(srcTex).Image, VkTextures.at(dstTex).Image, 1, &copyInfo);
}

void CVulkanDriver::copyFramebufferColorToBuffer(ShaderBufferHandle dstBuffer,
  const glm::ivec2& srcOffset, const std::uint64_t dstOffset, const glm::ivec2& size)
{
  VezBufferImageCopy copyInfo{};
  copyInfo.imageExtent.width = size.x;
  copyInfo.imageExtent.height = size.y;
  copyInfo.imageSubresource.mipLevel = 0;
  copyInfo.imageSubresource.baseArrayLayer = 0;
  copyInfo.imageSubresource.layerCount = 1;
  copyInfo.bufferOffset = dstOffset;

  vezCmdCopyImageToBuffer(DefaultFramebuffer.colorImage, VkShaderBuffers.at(dstBuffer), 1, &copyInfo);
}

void CVulkanDriver::copyFramebufferDepthToBuffer(ShaderBufferHandle dstBuffer,
  const glm::ivec2& srcOffset, const std::uint64_t dstOffset, const glm::ivec2& size)
{
  VezBufferImageCopy copyInfo{};
  copyInfo.imageExtent.width = size.x;
  copyInfo.imageExtent.height = size.y;
  copyInfo.imageSubresource.mipLevel = 0;
  copyInfo.imageSubresource.baseArrayLayer = 0;
  copyInfo.imageSubresource.layerCount = 1;
  copyInfo.bufferOffset = dstOffset;

  vezCmdCopyImageToBuffer(DefaultFramebuffer.depthStencilImage, VkShaderBuffers.at(dstBuffer), 1, &copyInfo);
}

void CVulkanDriver::blitTexture(
  TextureHandle srcTex, TextureHandle dstTex,
  const glm::ivec2& srcOffset, const glm::ivec2& dstOffset)
{
  VezImageBlit blitInfo = {};
  blitInfo.srcSubresource.mipLevel = 0;
  blitInfo.srcSubresource.baseArrayLayer = 0;
  blitInfo.srcSubresource.layerCount = 1;
  blitInfo.srcOffsets[1].x = srcOffset.x != 0 ? srcOffset.x : Textures.at(srcTex).Width;
  blitInfo.srcOffsets[1].y = srcOffset.y != 0 ? srcOffset.y : Textures.at(srcTex).Height;
  blitInfo.srcOffsets[1].z = 1;

  blitInfo.dstSubresource.mipLevel = 0;
  blitInfo.dstSubresource.baseArrayLayer = 0;
  blitInfo.dstSubresource.layerCount = 1;
  blitInfo.dstOffsets[1].x = dstOffset.x != 0 ? dstOffset.x : Textures.at(dstTex).Width;
  blitInfo.dstOffsets[1].y = dstOffset.y != 0 ? dstOffset.y : Textures.at(dstTex).Height;
  blitInfo.dstOffsets[1].z = 1;

  vezCmdBlitImage(VkTextures.at(srcTex).Image, VkTextures.at(dstTex).Image, 1, &blitInfo, VK_FILTER_LINEAR);
}

void CVulkanDriver::copyTextureToBuffer(TextureHandle srcTex, ShaderBufferHandle dstBuffer,
  const glm::ivec2& srcOffset, const std::uint64_t dstOffset, const glm::ivec2& size)
{
  VezBufferImageCopy copyInfo{};
  copyInfo.imageExtent.width = size.x;
  copyInfo.imageExtent.height = size.y;
  copyInfo.imageSubresource.mipLevel = 0;
  copyInfo.imageSubresource.baseArrayLayer = 0;
  copyInfo.imageSubresource.layerCount = 1;
  copyInfo.bufferOffset = dstOffset;
  // copyInfo.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

  // VkImageSubresource subResource { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0 };
  // VkSubresourceLayout subResourceLayout;
  // vkGetImageSubresourceLayout(VKDevice, VkTextures.at(srcTex).Image, &subResource, &subResourceLayout);

  vezCmdCopyImageToBuffer(VkTextures.at(srcTex).Image, VkShaderBuffers.at(dstBuffer), 1, &copyInfo);
}

VkImage CVulkanDriver::getVkTexture(const TextureHandle t) const
{
  return VkTextures.at(t).Image;
}

} // namespace video
} // namespace saga
