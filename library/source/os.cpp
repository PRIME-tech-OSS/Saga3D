// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "os.h"


#include "irrMath.h"

#if defined(_IRR_COMPILE_WITH_SDL_DEVICE_)
  #include <SDL2/SDL_endian.h>
  #define bswap_16(X) SDL_Swap16(X)
  #define bswap_32(X) SDL_Swap32(X)
#elif defined(_IRR_WINDOWS_API_) && defined(_MSC_VER) && (_MSC_VER > 1298)
  #include <stdlib.h>
  #define bswap_16(X) _byteswap_ushort(X)
  #define bswap_32(X) _byteswap_ulong(X)
#if (_MSC_VER >= 1400)
  #define localtime _localtime_s
#endif
#elif defined(_IRR_OSX_PLATFORM_)
  #include <libkern/OSByteOrder.h>
  #define bswap_16(X) OSReadSwapInt16(&X,0)
  #define bswap_32(X) OSReadSwapInt32(&X,0)
#elif defined(__FreeBSD__) || defined(__OpenBSD__)
  #include <sys/endian.h>
  #define bswap_16(X) bswap16(X)
  #define bswap_32(X) bswap32(X)
#elif !defined(_IRR_SOLARIS_PLATFORM_) && !defined(__PPC__) && !defined(_IRR_WINDOWS_API_)
  #include <byteswap.h>
#else
  #define bswap_16(X) ((((X)&0xFF) << 8) | (((X)&0xFF00) >> 8))
  #define bswap_32(X) ((((X)&0x000000FF)<<24) | (((X)&0xFF000000) >> 24) | (((X)&0x0000FF00) << 8) | (((X) &0x00FF0000) >> 8))
#endif

namespace saga
{
namespace os
{
  std::uint16_t Byteswap::byteswap(std::uint16_t num) {return bswap_16(num);}
  std::int16_t Byteswap::byteswap(std::int16_t num) {return bswap_16(num);}
  std::uint32_t Byteswap::byteswap(std::uint32_t num) {return bswap_32(num);}
  std::int32_t Byteswap::byteswap(std::int32_t num) {return bswap_32(num);}
  float Byteswap::byteswap(float num) {std::uint32_t tmp=IR(num); tmp=bswap_32(tmp); return (FR(tmp));}
  // prevent accidental byte swapping of chars
  u8  Byteswap::byteswap(u8 num)  {return num;}
  char  Byteswap::byteswap(char num)  {return num;}
}
}

#if defined(_IRR_WINDOWS_API_)
// ----------------------------------------------------------------
// Windows specific functions
// ----------------------------------------------------------------

#ifdef _IRR_XBOX_PLATFORM_
#include <xtl.h>
#else
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <time.h>
#endif

namespace saga
{
namespace os
{
  //! prints a debuginfo string
  void Printer::print(const char* message)
  {
#if defined (_WIN32_WCE)
    std::wstring tmp(message);
    tmp += L"\n";
    OutputDebugStringW(tmp.c_str());
#else
    std::string tmp(message);
    tmp += "\n";
    OutputDebugStringA(tmp.c_str());
    printf("%s", tmp.c_str());
#endif
  }

  static LARGE_INTEGER HighPerformanceFreq;
  static BOOL HighPerformanceTimerSupport = FALSE;
  static BOOL MultiCore = FALSE;

  void Timer::initTimer(bool usePerformanceTimer)
  {
#if !defined(_WIN32_WCE) && !defined (_IRR_XBOX_PLATFORM_)
    // workaround for hires timer on multiple core systems, bios bugs result in bad hires timers.
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);
    MultiCore = (sysinfo.dwNumberOfProcessors > 1);
#endif
    if (usePerformanceTimer)
      HighPerformanceTimerSupport = QueryPerformanceFrequency(&HighPerformanceFreq);
    else
      HighPerformanceTimerSupport = FALSE;
    initVirtualTimer();
  }

  std::uint32_t Timer::getRealTime()
  {
    if (HighPerformanceTimerSupport)
    {
#if !defined(_WIN32_WCE) && !defined (_IRR_XBOX_PLATFORM_)
      // Avoid potential timing inaccuracies across multiple cores by
      // temporarily setting the affinity of this process to one core.
      DWORD_PTR affinityMask= 0;
      if(MultiCore)
        affinityMask = SetThreadAffinityMask(GetCurrentThread(), 1);
#endif
      LARGE_INTEGER nTime;
      BOOL queriedOK = QueryPerformanceCounter(&nTime);

#if !defined(_WIN32_WCE)  && !defined (_IRR_XBOX_PLATFORM_)
      // Restore the true affinity.
      if(MultiCore)
        (void)SetThreadAffinityMask(GetCurrentThread(), affinityMask);
#endif
      if(queriedOK)
        return std::uint32_t((nTime.QuadPart) * 1000 / HighPerformanceFreq.QuadPart);

    }

    return GetTickCount();
  }

} // namespace os


#else

// ----------------------------------------------------------------
// linux/ansi version
// ----------------------------------------------------------------

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

namespace saga
{
namespace os
{

  //! prints a debuginfo string
  void Printer::print(const char* message)
  {
    printf("%s\n", message);
  }

  void Timer::initTimer(bool usePerformanceTimer)
  {
    initVirtualTimer();
  }

  std::uint32_t Timer::getRealTime()
  {
    timeval tv;
    gettimeofday(&tv, 0);
    return (std::uint32_t)(tv.tv_sec * 1000) + (tv.tv_usec / 1000);
  }
} // namespace os

#endif // end linux / windows

namespace os
{
  // The platform independent implementation of the printer
  ILogger* Printer::Logger = 0;

  void Printer::log(const char* message, ELOG_LEVEL ll)
  {
    if (Logger)
      Logger->log(message, ll);
  }

  void Printer::log(const char* message, ELOG_LEVEL ll)
  {
    if (Logger)
      Logger->log(message, ll);
  }

  void Printer::log(const char* message, const char* hint, ELOG_LEVEL ll)
  {
    if (Logger)
      Logger->log(message, hint, ll);
  }

  void Printer::log(const char* message, const std::string& hint, ELOG_LEVEL ll)
  {
    if (Logger)
      Logger->log(message, hint.c_str(), ll);
  }

  // our Randomizer is not really os specific, so we
  // code one for all, which should work on every platform the same,
  // which is desirable.

  std::int32_t Randomizer::seed = 0x0f0f0f0f;

  //! generates a pseudo random number
  std::int32_t Randomizer::rand()
  {
    // (a*seed)%m with Schrage's method
    seed = a * (seed%q) - r* (seed/q);
    if (seed<1)
      seed += m;

    return seed-1;  // -1 because we want it to start at 0
  }

  //! generates a pseudo random number
  float Randomizer::frand()
  {
    return rand()*(1.f/rMax);
  }

  std::int32_t Randomizer::randMax()
  {
    return rMax;
  }

  //! resets the randomizer
  void Randomizer::reset(std::int32_t value)
  {
    if (value<0)
      seed = value+m;
    else if (value == 0 || value == m)
      seed = 1;
    else
      seed = value;
  }


  // ------------------------------------------------------
  // virtual timer implementation

  float Timer::VirtualTimerSpeed = 1.0f;
  std::int32_t Timer::VirtualTimerStopCounter = 0;
  std::uint32_t Timer::LastVirtualTime = 0;
  std::uint32_t Timer::StartRealTime = 0;
  std::uint32_t Timer::StaticTime = 0;

  //! Get real time and date in calendar form
  ITimer::RealTimeDate Timer::getRealTimeAndDate()
  {
    time_t rawtime;
    time(&rawtime);

    struct tm * timeinfo;
    timeinfo = localtime(&rawtime);

    // init with all 0 to indicate error
    ITimer::RealTimeDate date;
    memset(&date, 0, sizeof(date));
    // at least Windows returns NULL on some illegal dates
    if (timeinfo)
    {
      // set useful values if succeeded
      date.Hour=(std::uint32_t)timeinfo->tm_hour;
      date.Minute=(std::uint32_t)timeinfo->tm_min;
      date.Second=(std::uint32_t)timeinfo->tm_sec;
      date.Day=(std::uint32_t)timeinfo->tm_mday;
      date.Month=(std::uint32_t)timeinfo->tm_mon+1;
      date.Year=(std::uint32_t)timeinfo->tm_year+1900;
      date.Weekday=(ITimer::EWeekday)timeinfo->tm_wday;
      date.Yearday=(std::uint32_t)timeinfo->tm_yday+1;
      date.IsDST=timeinfo->tm_isdst != 0;
    }
    return date;
  }

  //! returns current virtual time
  std::uint32_t Timer::getTime()
  {
    if (isStopped())
      return LastVirtualTime;

    return LastVirtualTime + (std::uint32_t)((StaticTime - StartRealTime) * VirtualTimerSpeed);
  }

  //! ticks, advances the virtual timer
  void Timer::tick()
  {
    StaticTime = getRealTime();
  }

  //! sets the current virtual time
  void Timer::setTime(std::uint32_t time)
  {
    StaticTime = getRealTime();
    LastVirtualTime = time;
    StartRealTime = StaticTime;
  }

  //! stops the virtual timer
  void Timer::stopTimer()
  {
    if (!isStopped())
    {
      // stop the virtual timer
      LastVirtualTime = getTime();
    }

    --VirtualTimerStopCounter;
  }

  //! starts the virtual timer
  void Timer::startTimer()
  {
    ++VirtualTimerStopCounter;

    if (!isStopped())
    {
      // restart virtual timer
      setTime(LastVirtualTime);
    }
  }

  //! sets the speed of the virtual timer
  void Timer::setSpeed(float speed)
  {
    setTime(getTime());

    VirtualTimerSpeed = speed;
    if (VirtualTimerSpeed < 0.0f)
      VirtualTimerSpeed = 0.0f;
  }

  //! gets the speed of the virtual timer
  float Timer::getSpeed()
  {
    return VirtualTimerSpeed;
  }

  //! returns if the timer currently is stopped
  bool Timer::isStopped()
  {
    return VirtualTimerStopCounter < 0;
  }

  void Timer::initVirtualTimer()
  {
    StaticTime = getRealTime();
    StartRealTime = StaticTime;
  }

} // namespace os
} // namespace saga


