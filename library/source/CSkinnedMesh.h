#ifndef __C_SKINNED_MESH_H_INCLUDED__
#define __C_SKINNED_MESH_H_INCLUDED__

#include "ISkinnedMesh.h"
#include "SMesh.h"
#include <assimp/Importer.hpp>
#include <assimp/matrix4x4.h>
#include <assimp/anim.h>
#include <assimp/scene.h>
#include <unordered_map>

namespace saga
{
namespace scene
{

  struct SVertexBoneData
  {
    std::array<uint32_t, MAX_BONES_PER_VERTEX> IDs;
    std::array<float, MAX_BONES_PER_VERTEX> Weights;

    void add(uint32_t boneID, float weight)
    {
      for (uint32_t i = 0; i < MAX_BONES_PER_VERTEX; i++)
      {
        if (Weights[i] == 0.0f)
        {
          IDs[i] = boneID;
          Weights[i] = weight;
          return;
        }
      }
    }
  };

  struct SBoneInfo
  {
    aiMatrix4x4 Offset;
    aiMatrix4x4 FinalTransformation;

    SBoneInfo()
    {
      Offset = aiMatrix4x4();
      FinalTransformation = aiMatrix4x4();
    };
  };

  class CSkinnedMesh : public ISkinnedMesh
  {
  public:

    //! constructor
    CSkinnedMesh();

    //! destructor
    virtual ~CSkinnedMesh();

    virtual void addMeshBuffer(std::unique_ptr<IMeshBuffer> buf) override
    { Mesh->addMeshBuffer(std::move(buf)); }

    virtual std::uint32_t getMeshBufferCount() const override
    { return Mesh->getMeshBufferCount(); }

    virtual IMeshBuffer& getMeshBuffer(std::uint32_t nr = 0) override
    { return Mesh->getMeshBuffer(nr); }

    virtual const core::aabbox3d<float>& getBoundingBox() const override
    { return Mesh->getBoundingBox(); }

    virtual void setBoundingBox(const core::aabbox3df& box) override
    { Mesh->setBoundingBox(box); }

    virtual E_MESH_TYPE getMeshType() const override { return E_MESH_TYPE::SKINNED; }

    //! returns the amount of frames. If the amount is 1, it is a static (=non animated) mesh.
    virtual std::uint32_t getFrameCount() const override { return Scene->mNumAnimations; }

    //! Gets the default animation speed of the animated mesh.
    /** \return Amount of frames per second. If the amount is 0, it is a static, non animated mesh. */
    virtual float getAnimationSpeed() const override { return AnimationSpeed; }

    //! Gets the frame count of the animated mesh.
    /** \param fps Frames per second to play the animation with. If the amount is 0, it is not animated.
    The actual speed is set in the scene node the mesh is instantiated in.*/
    virtual void setAnimationSpeed(float fps) override { AnimationSpeed = fps; }

    //! Gets joint count.
    virtual std::uint32_t getBoneCount() const override { return BoneCount; }

    //! Gets transformation matrix of bone
    /** \return 4x4 matrix representing bone transformation in current animation. */
    virtual glm::mat4 getBoneTransform(const std::uint32_t boneID) const override;

    //! Gets the name of a joint.
    // virtual const std::string& getJointName(std::uint32_t index) const override;

    //! Gets a joint number from its name
    // virtual std::int32_t getBoneID(const std::string& name) const override;

    //! uses animation from another mesh
    // virtual bool useAnimationFrom(const ISkinnedMesh *mesh) override;

    // virtual void updateBoundingBox(void);
    virtual void setAnimation(const uint32_t index) override
    {
      if (index < Scene->mNumAnimations)
      {
        Animation = Scene->mAnimations[index];
      }
    }

    virtual void onAnimate(const float time) override;

    auto& getMesh() { return Mesh; }

    std::unordered_map<std::string, uint32_t> BoneMapping;
    std::vector<SBoneInfo> BoneInfo;
    uint32_t BoneCount = 0;
    aiMatrix4x4 GlobalInverseTransform = aiMatrix4x4();
    std::vector<SVertexBoneData> Bones;
    std::vector<aiMatrix4x4> BoneTransforms;
    float AnimationSpeed = 0.75f;
    aiAnimation* Animation = nullptr;
    const aiScene* Scene = nullptr;
    std::shared_ptr<SMesh> Mesh = nullptr;

    void loadBones(const aiMesh* pMesh, uint32_t vertexOffset);

  private:
  	const aiNodeAnim* findNodeAnim(const aiAnimation* animation, const std::string nodeName);
    aiMatrix4x4 interpolateTranslation(float time, const aiNodeAnim* pNodeAnim);
	  aiMatrix4x4 interpolateRotation(float time, const aiNodeAnim* pNodeAnim);
	  aiMatrix4x4 interpolateScale(float time, const aiNodeAnim* pNodeAnim);
    void readNodeHierarchy(float AnimationTime, const aiNode* pNode, const aiMatrix4x4& ParentTransform);
  };

} // namespace scene
} // namespace saga

#endif

