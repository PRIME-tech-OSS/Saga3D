#include "CSagaDeviceStub.h"
#include "CSceneManager.h"

namespace saga
{
//! constructor
CSagaDeviceStub::CSagaDeviceStub(const SDeviceCreationParameters& params)
:  SagaDevice(), // VideoDriver(0), // SceneManager(0),
   CreationParams(params), Close(false)
{

}

CSagaDeviceStub::~CSagaDeviceStub()
{

}

//! add event listener
void CSagaDeviceStub::addEventReceiver(IEventReceiver* receiver)
{
  if (receiver) EventReceivers.push_back(receiver);
}

// ! creates scene manager
void CSagaDeviceStub::createSceneManager()
{
  SceneManager = std::make_unique<scene::CSceneManager>(VideoDriver);
}

//! Checks if the window is running in fullscreen mode
bool CSagaDeviceStub::isFullscreen() const
{
  return CreationParams.Fullscreen;
}

} // namespace saga

